package nz.ac.massey.cs.sinkreachability;

import com.google.common.base.Preconditions;
import org.apache.commons.compress.compressors.CompressorInputStream;
import org.apache.commons.compress.compressors.CompressorStreamFactory;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.jgrapht.alg.interfaces.VertexScoringAlgorithm;
import org.jgrapht.alg.scoring.PageRank;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import java.io.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Script to prepare the twitter dataset from http://konect.uni-koblenz.de/downloads/tsv/twitter_mpi.tar.bz2 into a sink graph.
 * Arguments are the input file, and the output file, there are several constants in this class to customise the script further.
 * @author jens dietrich
 */
public class PrepareTwitterDataset {

    private static final Logger LOGGER = Logger.getLogger("prep-twitter-dataset");
    private static final int FIRST_DATA_LINE = 63;
    private static final int MAX_EDGE_COUNT = 10_000_000;
    private static final int TOP_INFLUENCERS_TO_SELECT = 10_000;
    private static final int PAGE_RANK_ITERATIONS = 10;

    public static void main(String[] args) throws Exception {

        PropertyConfigurator.configure("log4j.properties");

        Preconditions.checkArgument(args.length==2,"Two arguments required, input (file downloaded from http://konect.uni-koblenz.de/downloads/tsv/twitter_mpi.tar.bz2), and output (tsv file)");
        File input = new File(args[0]);
        Preconditions.checkArgument(input.exists());
        File output = new File(args[1]);

        LOGGER.info("Reading input from " + input.getAbsolutePath());

        FileInputStream fin = new FileInputStream(input);
        BufferedInputStream bis = new BufferedInputStream(fin);
        CompressorInputStream cis = new CompressorStreamFactory().createCompressorInputStream(bis);
        BufferedReader br = new BufferedReader(new InputStreamReader(cis));

        AtomicInteger counter = new AtomicInteger();
        DefaultDirectedGraph<String, DefaultEdge> graph = new DefaultDirectedGraph<>(DefaultEdge.class);

        LOGGER.info("Importing graph, max edge count is " + MAX_EDGE_COUNT + " -- all other edges will be ignored");
        br.lines().limit(MAX_EDGE_COUNT+FIRST_DATA_LINE-1).forEach( line -> {
                if (counter.incrementAndGet() % 1_000_000 == 0) {
                    LOGGER.info("\tlines processed: " + counter.get());
                }

                // inspect this to see where comment ends
                //                if (counter.get()<200) {
                //                    System.out.println(counter.get() + "\t:\t" + line);
                //                }

                if (counter.get()>=FIRST_DATA_LINE) {
                    String[] tokens = line.split(" ");
                    assert tokens.length == 2;

                    int source = Integer.parseInt(tokens[0]);
                    int sink = Integer.parseInt(tokens[1]);

                    graph.addVertex("v"+source);
                    graph.addVertex("v"+sink);
                    graph.addEdge("v"+source, "v"+sink);
                }
            }
        );

        LOGGER.info("Graph built: " + graph.vertexSet().size() + " vertices, " + graph.edgeSet().size() + " edges");

        LOGGER.info("Computing page rank to find influencer vertices, using " + PAGE_RANK_ITERATIONS + " iterations" );
        VertexScoringAlgorithm<Integer,Double> pageRank = new PageRank(graph, 0.9, PAGE_RANK_ITERATIONS);
        Map<Integer,Double> scores = pageRank.getScores();


        LOGGER.info("Page rank computed");
        LOGGER.info("Sorting vertices by page rank");
        Comparator<String> comp = (v1,v2) -> {
            double diff = scores.get(v1)-scores.get(v2);
            return diff > 0.0 ? -1 : 1;
        };
        Set<String> verticesByPageRank = new TreeSet<>(comp);
        verticesByPageRank.addAll(graph.vertexSet());

        LOGGER.info("Selecting top " + TOP_INFLUENCERS_TO_SELECT + " influencers");
        Collection<String> topInfluencers = verticesByPageRank.stream().limit(TOP_INFLUENCERS_TO_SELECT).collect(Collectors.toList());

        // for debugging only
        LOGGER.info("\tprinting details of some top influencers");
        topInfluencers.stream().limit(20).forEach( v -> {
            LOGGER.debug("\t\t" + v + " -- score: " + scores.get(v) + " , indegree: " + graph.inDegreeOf(v));
        });

        LOGGER.info("Adding sink nodes for top influencers");
        AtomicInteger sinkCounter = new AtomicInteger();
        topInfluencers.stream().forEach(v -> {
            String sink = "s" + sinkCounter.incrementAndGet();
            graph.addVertex(sink);
            graph.addEdge(v,sink);
        });

        LOGGER.info("Exporting new graph to " + output.getAbsolutePath());
        try (PrintStream out = new PrintStream(new FileOutputStream(output))) {
            graph.edgeSet().stream().forEach( e -> {
                out.print(graph.getEdgeSource(e));
                out.print('\t');
                out.println(graph.getEdgeTarget(e));
            });
        }
        LOGGER.info("Done");


        LOGGER.debug("Debugging some data:");
        debug(graph,"v285087",scores);
        debug(graph,"v250991",scores);

    }


    // print the nodes depending on a vertex
    private static void debug(DefaultDirectedGraph<String, DefaultEdge> graph,String vertex,Map<Integer,Double> scores) {
        Set<String> visited = new HashSet<>();
        debug(graph,vertex,scores,visited,0);
    }

    private static void debug(DefaultDirectedGraph<String, DefaultEdge> graph, String vertex, Map<Integer,Double> scores,Set<String> visited, int depth) {
        if (depth<5) {
            boolean alreadySeen = !visited.add(vertex);
            String s = "\t";
            for (int i=0;i<depth;i++) {
                s = s + "\t";
            }
            LOGGER.debug(s + vertex + " -- score: " + scores.get(vertex) + " , indegree: " + graph.inDegreeOf(vertex) + " , outdegree: " + graph.outDegreeOf(vertex) + (alreadySeen?"\t*":""));

            if (!alreadySeen) {
                graph.incomingEdgesOf(vertex).stream().map(e -> graph.getEdgeSource(e)).forEach(next ->
                        debug(graph, next, scores, visited, depth + 1)
                );
            }
        }
    }
}
